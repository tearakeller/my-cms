// Vuetify Documentation https://vuetifyjs.com

import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'
import ripple from 'vuetify/lib/directives/ripple'

Vue.use(Vuetify, { directives: { ripple } })

const theme = {
  primary: '#00aeef', // $kcm-blue
  secondary: '#73c45a', // $kcm-green
  accent: '#ff8300', // $kcm-orange
  info: '#00CAE3',
  success: '#598043',
  warning: '#ffde59',
  error: '#e93e51',
}

export default new Vuetify({
  breakpoint: { mobileBreakpoint: 960 },
  icons: {
    values: { expand: 'mdi-menu-down' },
  },
  theme: {
    themes: {
      dark: theme,
      light: theme,
    },
  },
})
