import Vue from 'vue'
import App from './App.vue'
import axiosApi from 'axios'
import router from './router'
import vuetify from './plugins/vuetify'
import './plugins'
import store from './store'
import { sync } from 'vuex-router-sync'
import { TiptapVuetifyPlugin } from 'tiptap-vuetify'

const axios = axiosApi.create({
  baseURL: 'http://localhost:8000/api/',
  headers: { header: [] },
})

Vue.use(TiptapVuetifyPlugin, { vuetify, iconsGroup: 'mdi' })

Vue.config.productionTip = false

sync(store, router)

new Vue({
  axios,
  router,
  vuetify,
  store,
  render: h => h(App),
}).$mount('#app')
