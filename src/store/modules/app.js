// Pathify
import { make } from 'vuex-pathify'

// Data
const state = {
  drawer: null,
  mini: false,
  items: [
    {
      title: 'Dashboard',
      icon: 'mdi-view-dashboard',
      to: '/',
    },
    {
      title: 'Content',
      icon: 'mdi-pencil',
      to: '/components/edit-post/',
    },
    {
      title: 'Media Library',
      icon: 'mdi-movie-open',
      to: '/components/media-library/',
    },
    {
      title: 'Content Types',
      icon: 'mdi-text-box-outline',
      to: '/components/content-types/',
    },
    {
      title: 'Categories',
      icon: 'mdi-chart-bubble',
      to: '/components/categories/',
    },
    {
      title: 'Admin',
      icon: 'mdi-shield-star-outline',
      to: '/components/admin/',
    },
    {
      title: 'User Profile',
      icon: 'mdi-account',
      to: '/components/profile/',
    },
  ],
}

const mutations = make.mutations(state)

const actions = {
  ...make.actions(state),
  init: async ({ dispatch }) => {
    //
  },
}

const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
