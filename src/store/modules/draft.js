// Utilities
import { make } from 'vuex-pathify'

// Globals
import { IN_BROWSER } from '@/util/globals'

const state = {
  postTitle: 'Title',
  postContent: 'Post Content',
  postTags: [
    'tag 1', 'tag 2', 'tag 3',
  ],
  postCategory: [
    'category 1', 'category 2', 'category 3',
  ],
  contentType: 'blogs',
  postStatus: 'draft',
  postVisibility: 'public',
  postSchedule: 'date', // Should be a terniary statement for if it's not published or is scheduled
  postFeatureImg: 'image.jpg', // Should be a terniary statement for posts without featured images
}

const mutations = make.mutations(state)

const actions = {
  fetch: ({ commit }) => {
    const local = localStorage.getItem('vuetify@posts') || '{}'
    const posts = JSON.parse(local)

    for (const key in posts) {
      commit(key, posts[key])
    }
  },
  update: ({ state }) => {
    if (!IN_BROWSER) return

    localStorage.setItem('vuetify@posts', JSON.stringify(state))
  },
}

const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
